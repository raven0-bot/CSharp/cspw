﻿using System;
using FluentColorConsole;

namespace cspw
{
    class Program
    {
        public static void Main(string[] args)
        {
            name:
            ColorConsole.WithYellowText.WriteLine("Please enter a name...");
            string input_name = Console.ReadLine();
            string name = input_name.Trim();

            if (name == "") {
                ColorConsole.WithRedText.WriteLine("No name entered... Try again!");
                goto name;
                }

            username:
            ColorConsole.WithYellowText.WriteLine("Please enter a username...");
            string input_username = Console.ReadLine();
            string username = input_username.Trim();

            if (username == "") {
                ColorConsole.WithRedText.WriteLine("No username entered... Try again!");
                goto username;
            }

            password:
            ColorConsole.WithYellowText.WriteLine("Please enter a password...");
            string input_password = Console.ReadLine();
            string password = input_password.Trim();

            if (password == "") {
                ColorConsole.WithRedText.WriteLine("No password entered... Try again!");
                goto password;
            }
            ColorConsole.WithGreenText.WriteLine("Name: " + name + ", Username: " + username + ", Password: " + password);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey(true);
        }
    }
}
